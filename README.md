# Rapport projet Optimisation de portefeuille

Rendu par M. Théophile Dano

## Méthode des treillis

Tout d’abord, je souhaite détailler mon implémentation : j’ai implémenté mon modèle de pricing en _« Go »_, un langage compilé. La méthode de calcul repose sur un algorithme récursif. Cependant, pour limiter l’impact de la récursion et augmenter les performances, j’ai ajouté une table de _« mémoïsation »_. Chaque résultat intermédiaire est gardé en mémoire afin de ne pas être recalculé lors de la prochaine étape du pricing.

#### Analyse du temps de calcul

Cette méthode m’a apporté un temps qui semble être polynomial ou alors exponentiel avec un coefficient < 2 en fonction de N comme peuvent en attester les graphes ci-dessous. On observe d’ailleurs que les options américaines ont un temps de calcul relativement plus élevé que les européennes bien que plus « stable » autour de sa moyenne lissée.

![Temps en fonction de N](q1/temps_f_N_eu.PNG)

#### Analyse en fonction du taux d'intérêt

Première chose que l'on remarque : les option CALL européennes et américaines ont le même prix peu importe le taux d'intérêt. Il s'agit en fait de la conséquence immédiate de la **théorie du marché complet**. Si le taux d'intérêt sans risque (_risk free_) est positif alors les prix seront égaux. En effet, même si l'on éxerce notre droit d'achat (option call américaine) avant le terme alors on investi directement notre argent dans une action qui, sous probabilité risque-neutre (marché complet) aura le même rendement. Les PUT n'ont pas cette propriété car la formule du payoff est différente.

![Prix en fonction de r](q1/prix_f_r.PNG)

Ici, on observe bien que le prix d'un CALL est correlé linéairement au taux d'intérêt sans risque du marché. A l'inverse, le prix des options PUT américaines et européennes se comporte inversement proportionnel à ce taux. En effet, plus le taux sans risque du marché est élevé, moins investir son argent dans l'option est intéréssant.

#### Analyse en fonction de u

En faisant varier `u` de 1 à 2, on observe le même comportement pour les CALL ainsi que les PUT, européens ou américains. Le PUT est toujours moins cher que le CALL, et les quatre actifs ont un prix proportionnel au $`log(u)`$.

Options européennes |  Options américaines
:-------------------------:|:-------------------------:
![Prix en fonction de u](q1/prix_f_u_EU.PNG) | ![Prix en fonction de u](q1/prix_f_u_US.PNG)

## Minimisation de la CVaR

#### Modèle
Premièrement, on se donne le modèle qui permet de minimiser la CVaR pour une probabilité donnée $`\alpha`$.

```math
(P1) \begin{cases}
   \min \gamma + \frac{1}{(1-\alpha)S} \sum_{s = 1}^{S} z_{s} & \\
   z_{s} \ge \sum (b_i - y_i)x_i - \gamma & \text{pour } s = 1, ..., S \\
   z_{s} \ge 0 & \text{pour } s = 1, ..., S \\
   \sum_i x_i = 1 & \\
   \sum_i \mu_i x_i \ge R
\end{cases}
```

#### Données aléatoires

Pour résoudre ce problème, je suis passé par la bibliothèque Scikit-optimize en python. En choisissant des valeurs pour certains paramètres comme rapporté dans le tableau ci-dessous, j'ai pu observer que l'évolution de la CVaR et de la VaR est proportionnelle à la probabilité $`\alpha`$ ainsi qu'au niveau de rendement attendu. Les paramètres statiques sont : $`r = 0.05`$, $`N = 20`$ et $`S = 100`$.

| $`\alpha`$ | $`VaR`$ | $`CVaR`$ |
| ------ | ------ | ------ |
| $`0.1`$ | 143.23 | 98.16 |
| $`0.3`$ | 156.74 | 118.49 |
| $`0.5`$ | 194.18 | 157.93 |
| $`0.7`$ | 256.32 | 238.60 |
| $`0.8`$ | 281.86 | 265.14 |
| $`0.9`$ | 306.21 | 305.77 |
| $`0.99`$ | 306.21 | 306.21 |

Avec un rendu graphique :

![image.png](var_cvar_alpha.PNG)

On observe bien ici la proportionnalité évoquée plus tôt ainsi que le fait que $`CVaR > VaR`$.

Ensuite, j'ai fait jouer sur beaucoup plus de valeurs possibles l'incidence du niveau de rendement sur la VaR et la CVaR. Notons que les valeurs de VaR et de CVaR sont très très proches et que les valeurs sont exponentiellement croissantes avec $`R`$. Les paramètres fixés sont $`\alpha = 0.8`$ ainsi que les mêmes que précédemment.

| $`R`$ | $`VaR`$ | $`CVaR`$ |
| ------ | ------ | ------ |
| $`0.05`$ | 8.46 | 8.54 |
| $`0.1`$ | 9.82 | 9.92 |
| $`0.2`$ | 12.23 | 12.35 |
| $`0.4`$ | 16.71 | 16.88 |
| $`0.5`$ | 25.94 | 26.20 |
| $`0.6`$ | 47.75 | 48.23 |

![image-2.png](var_cvar_r.PNG)

#### Maximisation du rendement avec CVaR supérieure à un seuil

On donne ici le modèle pour maximiser le rendement, tout en s'assurant que la CVaR reste supérieure à un seuil donné.

```math
(P2) \begin{cases}
   \max \sum_i \mu_i x_i & \\
   \gamma + \frac{1}{(1-\alpha_j)S} \sum_{s = 1}^{S} z_{s} \le U_{\alpha_j} & \text{pour } j = 1, ..., J \\
   z_{s} \ge 0 - \gamma & \text{pour } s = 1, ..., S \\
   z_{s} \ge f(x, y_s) - \gamma & \text{pour } s = 1, ..., S \\
   x \in X
\end{cases}
```

On résout ensuite le modèle pour quelques valeurs.

| $`S`$ | $`U`$ | $`\mu`$ | $`\alpha`$ | $`CVaR`$ | $`VaR`$ |
| ------ | ------ | ------ | ------ | ------ | ------ |
| 500 | 15 | $`\begin{bmatrix} 0.23 \\ 0.86 \\ 0.44 \end{bmatrix}`$ | $`0.95`$ | $`0.754`$ | $`0.681`$ |
| 1000 | 10 | $`\begin{bmatrix} 0.58 \\ 0.42 \\ 0.71 \end{bmatrix}`$ | $`0.7`$ | $`0.655`$ | $`0.541`$ |
| 5000 | 6 | $`\begin{bmatrix} 0.23 \\ 0.86 \\ 0.76 \\ 0.14 \end{bmatrix}`$ | $`0.4`$ | $`0.415`$ | $`0.4142`$ |





